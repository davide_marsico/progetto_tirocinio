package controller;


import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		// gestione della RICHIESTA

		// leggo i parametri
		String file = request.getParameter("file");
		String nomeFile = file;

		String nextPage = null;

		HttpSession session = request.getSession();
		session.setAttribute("nomeFile",nomeFile);


		nextPage = "/fileScelto.jsp";

		ServletContext application  = getServletContext(); 
		RequestDispatcher rd = application.getRequestDispatcher(nextPage);
		rd.forward(request, response); 
		return;  
	}
}